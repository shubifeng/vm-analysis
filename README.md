# 数据分析工程说明
**1. html-api生成**  

-  **setting**添加**jcenter** 相关的配置,**启动**Web工程;  
-  Web工程上右键选择**Run Maven->Plugins**：依次执行 **Maven Helper**插件生成html格式api文档
    - `asciidoctor-maven-plugin`  
    - 查看生成文件：viomi-analysis-web\src\main\resources\static\docs  
-  访问api：[数据平台api](http://localhost:9063/docs/all.html)（本机启动）  
 http://HOST:9064/docs/all.html（其他环境修改**HOST**）


**2. 工程配置文件和部署说明**  

**主配置文件：**  
- **idea启动**的默认读取文件：     application.yml(指向application-default.yml)  
- **java命令启动**的默认读取文件： application-default.yml(指定不同环境配置文件)  

**主配置文件中`spring.profiles.active`指定具体使用的配置文件：**  
- **本地环境**启动指定文件：application-local.yml  
- **开发环境**启动指定文件：application-dev.yml  
- **测试环境**启动指定文件：application-test.yml  
- **生产环境**启动指定文件：application-prod.yml  
> 备注：
java命令指定配置文件启动 --spring.profiles.active=dev,但是需要修改相应配置文件.

**主配置文件中`spring.profiles.include`指定swagger等文件：**  
- swagger及生成API文档配置：application-md.yml 

**其他部署依赖文件：**  
- **本地环境**：IDE启动,执行WebApplication的main方法,依赖文件application.yml  
- **开发环境**：Jenkins部署,依赖文件Jenkinsfile_dev,Dockerfile,application-default.yml(**master**分支)  
- **测试环境**：Jenkins部署,依赖文件Jenkinsfile_test,Dockerfile,viomi-analysis-test.json,application-default.yml(**test**分支)  
- **生产环境**：Jenkins部署,依赖文件Jenkinsfile_prod,Dockerfile,viomi-analysis-prod.json,application-default.yml(**release**分支)  
> 备注：  
a. master和release分支的application-default.yml文件配置不同，区分不同环境.  
b. master合并到release分支的时候选择Squash commit, 然后还原application-default.yml文件为release版本, 最后再提交推送.



