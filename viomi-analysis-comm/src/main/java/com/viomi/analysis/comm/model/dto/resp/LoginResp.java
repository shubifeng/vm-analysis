package com.viomi.analysis.comm.model.dto.resp;

import com.viomi.base.model.dto.user.CacheUser;
import lombok.Data;

import java.io.Serializable;

/**
 * @author Shubifeng
 */
@Data
public class LoginResp implements Serializable {

    private String token;

    private CacheUser cacheUser;
}
