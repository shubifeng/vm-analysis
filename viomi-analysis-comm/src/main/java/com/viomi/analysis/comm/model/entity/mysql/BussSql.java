package com.viomi.analysis.comm.model.entity.mysql;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 业务SQL
 */
@Data
@NoArgsConstructor
@Entity
@Table(name = "buss_sql")
public class BussSql implements Serializable {

    @Id
    private Integer id;

    private String module;

    private String sqlKey;

    private String sqlText;

    private String status;

    private String createdBy;

    private String createdTime;

    private String remark;

}
