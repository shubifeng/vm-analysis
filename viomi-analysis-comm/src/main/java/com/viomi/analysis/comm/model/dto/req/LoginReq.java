package com.viomi.analysis.comm.model.dto.req;

import com.viomi.analysis.comm.constant.SourceType;
import com.viomi.base.model.dto.req.PageReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

import java.io.Serializable;

/**
 * Created by asus on 2018/3/29.
 */
@Data
@ApiModel(description = "用户登录")
public class LoginReq extends PageReq implements Serializable {

    @ApiModelProperty(value = "用户名")
    @NotBlank
    private String loginName;
    @ApiModelProperty(value = "联系电话,值为@时表示查询不存在手机号码的用户记录")
    @NotBlank
    private String pwd;

}



