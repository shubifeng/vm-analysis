package com.viomi.analysis.feign;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * 对该接口进行代理，可直接通过@Autowired注入使用
 * 通知Feign在调用该接口方法时要向Eureka中查询名为ea的服务，从而得到服务URL
 * Feign将方法签名中方法参数对象序列化为请求参数放到HTTP请求中的过程，是由编码器(Encoder)完成的。
 * 同理，将HTTP响应数据反序列化为java对象是由解码器(Decoder,默认委托给SpringMVC中的MappingJackson2HttpMessageConverter)完成的。
 * 当状态码不在200 ~ 300之间时ErrorDecoder才会被调用
 *
 * Spring Cloud应用在启动时，Feign会扫描标有@FeignClient注解的接口，生成代理，并注册到Spring容器中。生成代理时Feign会为每个接口方法创建一个RequetTemplate对象，该对象封装了HTTP请求需要的全部信息
 *
 * @Author: luocj
 * @Date: Created in 2018/1/9 14:14
 * @Modified:
 */
@FeignClient(name = "report")
public interface TestReportApi {

    /**
     * findTest
     *
     * @Author: luocj
     * @Date: Created in 2018/1/9 14:13
     * @param templateId
     * @Modified:
     */
    @RequestMapping(value = "/find", method = RequestMethod.GET)
    public void findTest(@PathVariable("templateId")Long templateId)throws Exception ;

    /**
     *  aggregateTest
     *  @GetMapping is a shortcut for @RequestMapping(method = RequestMethod.GET), but not support consumes attribute.
     *  PathVariable路径参数，RequestParam请求参数
     *
     * @Author: luocj
     * @Date: Created in 2018/1/9 14:12
     * @param templateId
     * @param fromObjId
     * @Modified:
     */
    @GetMapping(value = "/aggregate/{templateId}")
    public void aggregateTest(@PathVariable("templateId")Long templateId,
                              @RequestParam(value = "fromObjId", required = false) Long fromObjId);

    /**
     *  mapReduceApiTest
     *
     * @Author: luocj
     * @Date: Created in 2018/1/10 12:02
     * @param templateId
     * @Return: void
     * @Modified:
     */
    @RequestMapping(value = "/mapReduce", method = RequestMethod.GET)
    public void mapReduceApiTest(@RequestParam("templateId")Long templateId);

}
