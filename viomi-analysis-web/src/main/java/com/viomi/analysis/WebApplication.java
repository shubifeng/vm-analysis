package com.viomi.analysis;


import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.session.SessionAutoConfiguration;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.netflix.feign.EnableFeignClients;

/**
 * @author Shubifeng
 */
@SpringBootApplication(exclude = {SessionAutoConfiguration.class})
@EnableCaching
@EnableFeignClients(basePackages = "com.viomi.analysis.feign")
//@ImportResource({"classpath:disconf.xml"})
@Slf4j
public class WebApplication implements CommandLineRunner {

    public static void main(String[] args) throws InterruptedException {
        SpringApplication.run(WebApplication.class, args);
/*
        new SpringApplicationBuilder(WebApplication.class).web(true).run(args);
*/
    }

    @Override
    public void run(String... args) throws Exception {
        System.err.println("服务提供者------>>启动完毕");
    }


}


