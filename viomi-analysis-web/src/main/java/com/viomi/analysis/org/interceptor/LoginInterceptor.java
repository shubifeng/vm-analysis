package com.viomi.analysis.org.interceptor;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.viomi.analysis.web.BaseUserController;
import com.viomi.base.common.constant.CommRespCode;
import com.viomi.base.common.util.ResultResp;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/***
 * 用户登录拦截
 * add by shubifeng
 */
@Slf4j
@Component
public class LoginInterceptor extends HandlerInterceptorAdapter implements BaseUserController {


    private final String UTF8 = "UTF-8";

    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        Long userId = getCurrUserId();
        if (userId != null && userId != -1) {
            return true;
        }
        responseOutWithJson(response, ResultResp.fail(CommRespCode.TOKEN_ABSENT_EXCEPTION));
        return false;
    }


    /**
     * 以JSON格式输出
     *
     * @param response
     */
    private void responseOutWithJson(HttpServletResponse response, Object resultStr) {
        //将实体对象转换为JSON Object转换
        response.setCharacterEncoding(UTF8);
        response.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.append(objectMapper.writeValueAsString(resultStr));
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }
}
