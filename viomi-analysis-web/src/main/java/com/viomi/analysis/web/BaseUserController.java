package com.viomi.analysis.web;

import com.viomi.base.model.dto.user.CacheUser;
import io.swagger.annotations.Api;

/**
 * 会话相关信息
 * @author ShuBifeng
 */
@Api(hidden = true)
public interface BaseUserController {

    /**
     * 定义的应用ID
     */
    Integer SYS_APP_ID = 1000;

    /**
     * 定义的应用名称
     */
    String SYS_APP_NAME = "数据分析、报表平台";

    /**
     * 当前登录用户id
     */
    ThreadLocal<Long> SESSION_USERID = new ThreadLocal<Long>();

    /**
     * 用户token
     */
    ThreadLocal<String> SESSION_TOKEN = new ThreadLocal<String>();

    /**
     * 当前登录用户缓存信息
     */
    ThreadLocal<CacheUser> SESSION_USERINFO = new ThreadLocal<CacheUser>();

    /**
     * 获取缓存用户信息
     *
     * @return
     */
    default CacheUser getCacheUser() {
        return SESSION_USERINFO.get();
    }

    /**
     * 设置缓存用户信息
     * @return
     */
    default void setCacheUser(CacheUser cacheUser) {
        SESSION_USERINFO.set(cacheUser);
    }

    /**
     * 获取缓存渠道id
     *
     * @return
     */
    default Long getChannelId() {
        return getCacheUser().getChannelId();
    }

    /**
     * 获取缓存orgID
     *
     * @return
     */
    default Long getOrgId() {
        return getCacheUser().getOrgId();
    }

    /**
     * 获取当前用户信息
     * @return
     */
    default Long getCurrUserId() {
        return SESSION_USERID.get();
    }

    /**
     * 设置当前用户id
     * @return
     */
    default void setCurrUserId(Long userId) {
        SESSION_USERID.set(userId);
    }

    /**
     * 获取当前用户登陆token
     * @return
     */
    default String getToken() {
        return SESSION_TOKEN.get();
    }

    /**
     * 设置当前用户登陆token
     * @return
     */
    default void setToken(String token) {
        SESSION_TOKEN.set(token);
    }


    /**
     * 删除所有线程变量，避免出现内存和变量异常问题
     */
    default void remove() {
        SESSION_USERID.remove();
        SESSION_TOKEN.remove();
        SESSION_USERINFO.remove();
    }

}
