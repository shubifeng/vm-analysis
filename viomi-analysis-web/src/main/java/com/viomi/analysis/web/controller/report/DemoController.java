//package com.viomi.analysis.web.controller.report;
//
//import com.google.common.collect.Lists;
//import com.viomi.analysis.feign.TestApi;
//import BussSql;
//import com.viomi.analysis.web.BaseUserController;
//import com.viomi.analysis.web.controller.dto.req.OrderReq;
//import com.viomi.analysis.web.controller.dto.resp.OrderResp;
//import com.viomi.base.common.constant.CommRespCode;
//import com.viomi.base.common.util.ResultResp;
//import com.viomi.boot.myjdbc.core.MyJdbcTemplate;
//import com.viomi.boot.myjdbc.page.PageInfo;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import lombok.extern.slf4j.Slf4j;
//import org.hibernate.validator.constraints.NotBlank;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.util.Assert;
//import org.springframework.validation.annotation.Validated;
//import org.springframework.web.bind.annotation.*;
//
//import javax.validation.Valid;
//import javax.validation.constraints.Max;
//import java.math.BigDecimal;
//import java.util.Date;
//import java.util.List;
//import java.util.Map;
//
//
///**
// * Created by asus on 2017/12/28.
// */
//@Api(tags = "报表demo的api")
//@RestController
//@RequestMapping(value = "/demo")
//@Validated
//@Slf4j
//public class DemoController implements BaseUserController {
//
//    @Autowired
//    private TestApi testApi;
//    @Autowired
//    private MyJdbcTemplate myJdbcTemplate;
//
//    @ApiOperation(value = "ssss", nickname = "shubifeng")
//    @GetMapping(value = "xxxx")
//    public ResultResp<OrderResp> xxxx(OrderReq orderReq) {
//        //demoService.demo();
//        return ResultResp.ok();
//    }
//
//    @ApiOperation(value = "dto返回示例", nickname = "shubifeng")
//    @PostMapping(value = "dto")
//    public ResultResp<OrderResp> dto(@RequestBody @Valid OrderReq orderReq) {
//        System.out.println(orderReq);
//        OrderResp report = new OrderResp();
//        report.setOrderId(122003L);
//        report.setOrderTime(new Date());
//        report.setStatus("1");
//        report.setUserId(22);
//        return ResultResp.ok(report);
//    }
//
//    @GetMapping(value = "test2")
//    public ResultResp<Object> reportPageRes(@NotBlank(message = "不能为空") String num,
//                                            @Max(3) int count) {
//
//        return ResultResp.ok();
//    }
//
//
//    @GetMapping(value = "params")
//    public ResultResp<Object> params(@RequestParam String userName) {
//        log.info("listMap:{}", myJdbcTemplate.find("select * from vm_report.report_record where id=12593 "));
//        //通用查询
//        List<Map<String, Object>> listMap = myJdbcTemplate.find("select * from vm_report.report_record where id=12593 ");
//        log.info("listMap:{}", listMap);
//
//        //参数类型必须与数据库字段一致
//        List<Object> listParam = Lists.newArrayList();
//        listParam.add(12593);
//        listParam.add(15673);
//        List<Map<String, Object>> listMap2 = myJdbcTemplate.find("select * from vm_report.report_record where id=? and from_objid=?", listParam);
//        log.info("listMap2:{}", listMap2);
//
//        ////////////////////特殊in查询 参数必须SQL拼接后使用,或根据占位符in(?,?,?)
//        List<Map<String, Object>> listMap3 = myJdbcTemplate.find("select * from vm_report.report_record where id in (?) ", 12593);
//        log.info("listMap3:{}", listMap3);
//
//        List<Map<String, Object>> listMap4 = myJdbcTemplate.find("select * from vm_report.report_record where id in (?,?) ", 12593, 12594);
//        log.info("listMap4:{}", listMap4);
//
//
//        //指定任意实体类进行反射
//        //List<Test> list = myJdbcTemplate.find(Test.class, "select id from vm_report.report_record where id in (?,?) ", 1, 2);
//        //log.info("我是list<Test>：{}", list);
//
//        //查询某一列
//        Object o = myJdbcTemplate.getSingleValue("id", "select * from vm_report.report_record where id in (?,?)", new Integer[]{12593, 12594});
//        log.info("我是Object：{}", o);
//
//        //查询第一列
//        Object o1 = myJdbcTemplate.getSingleValue(1, "select * from vm_report.report_record where id in (?,?)", 12593, 12594);
//        log.info("我是Object1：{}", o1);
//
//        //查询并返回单个值
//        Long num = myJdbcTemplate.getSingleValue("select count(*) from vm_report.report_record where id in (?,?)", 12593, 12594);
//        log.info("我是Object2：{}", num);
//
//        //查询并返回单个值
//        BigDecimal so = myJdbcTemplate.getSingleValue("select sum(id) from vm_report.report_record where id in (?,?)", 12593, 12594);
//        log.info("我是Object3：{}", so);
//
//
//        try {
//
//            //查询并返回唯一，非唯一时会报错
//            Object x1 = myJdbcTemplate.findUnique("select * from vm_report.report_record where id in (?,?) ", 12593, 12594);
//            log.info("x1：{}", x1);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        //查询并返回唯一，非唯一时会报错
//        Object x2 = myJdbcTemplate.findUnique("select id from vm_report.report_record where id in (?) ", 12593);
//        log.info("x2：{}", x2);
//
//        //查询并返回第一行
//        Object x3 = myJdbcTemplate.findFirstRow("select * from vm_report.report_record where id in (?,?)", 12593, 12594);
//        log.info("x3：{}", x3);
//
//        //查询并返回某一列的值
//        List<Object> x4 = myJdbcTemplate.findSingleColumnValue("id", "select * from vm_report.report_record where id in (?,?)", 12593, 12594);
//        log.info("x4：{}", x4);
//
//        //分页
//        PageInfo<Map<String, Object>> pageResult = myJdbcTemplate.page(1, 20, "select * from vm_report.report_record where id < ?  ", 12893);
//        log.info("x5：{}", pageResult);
//
//        //分页
//        PageInfo<BussSql> pageResult12 = myJdbcTemplate.page(BussSql.class, 1, 20, "select * from vm_report.report_record where id < ?  ", 12893);
//        log.info("pageResult12:{}", pageResult12);
//        return ResultResp.ok("参数xxx");
//    }
//
//    @PostMapping(value = "apus")
//    public ResultResp<Object> apus(@RequestParam int num) {
//        return ResultResp.fail(CommRespCode.TOKEN_EXPIRED);
//    }
//
//    @PostMapping(value = "fail")
//    public ResultResp<Object> fail(@RequestParam String userName) {
//        return ResultResp.fail();
//    }
//
//    @GetMapping(value = "old")
//    public ResultResp<String> get() {
//        //获取token
//        String token = getToken();
//        Assert.isNull(token, "謝謝謝謝謝謝謝謝謝謝謝謝");
//        return ResultResp.ok(testApi.test("222"));
//    }
//
//
//
//    /*public static void main(String ... arg) {
//        Map<String, String> m = SqlMap.loadQueries("/sql/wares.xml");
//        ParsedSql parsedSql = NamedParameterUtils.parseSqlStatement(m.get("wares_sales_by_day"));
//        log.info(parsedSql.toString());
//    }*/
//}
