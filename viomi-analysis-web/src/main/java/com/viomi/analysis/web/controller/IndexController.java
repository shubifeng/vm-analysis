package com.viomi.analysis.web.controller;

import com.viomi.analysis.comm.model.dto.req.LoginReq;
import com.viomi.analysis.comm.model.dto.resp.LoginResp;
import com.viomi.base.common.util.ResultResp;
import com.viomi.base.model.dto.user.CacheUser;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * Created by asus on 2018/4/26.
 */
@Slf4j
@RestController
public class IndexController {

    @ApiOperation(value = "登录")
    @PostMapping(value = "login")
    public ResultResp<LoginResp> dto(@RequestBody @Valid LoginReq loginReq) {
        log.info("登录接口");
        //登录

        CacheUser cacheUser = new CacheUser();
        //-----------------设置cookie.(改造保留cookie加密的方式,实际上随机字符串就ok了,只是以后如有必要可以根据cookie计算出compid,userid)
        LoginResp resp = new LoginResp();
        resp.setCacheUser(cacheUser);
        resp.setToken("token");
        return ResultResp.ok(resp);
    }


    @ApiOperation(value = "登出")
    @PostMapping(value = "out")
    public void dto() {
        log.info("登录接口");
        CacheUser cacheUser = new CacheUser();
    }
}
